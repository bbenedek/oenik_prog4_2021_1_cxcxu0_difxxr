var interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic =
[
    [ "AsteroidTic", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#abb1724e956686856c07ad8ddb5c3d5d3", null ],
    [ "DoTurn", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a5edce9147f9ee08900aa484607442f79", null ],
    [ "GameOver", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#abef9efb80b42a1af41ff4c0178ac5dce", null ],
    [ "LanderBoost", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a6b6a47ed99efb0d7d6f71747075f1305", null ],
    [ "LanderTick", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#aca5829fb460861e06dfe41d7f12e56e3", null ],
    [ "LoadGame", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a610c9e320145076a492ef2daabaf8891", null ],
    [ "NewAnomaly", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a646398066a8dbbe369d399817835821b", null ],
    [ "NewAsteroid", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#abfeff54de24f377b5adab763fa3f90d6", null ],
    [ "SaveGame", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#ac99f1b8217bfc3be07b4a311c16aa59f", null ],
    [ "UpdateToplist", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a7c27343274876de18b861df6aa469b00", null ]
];