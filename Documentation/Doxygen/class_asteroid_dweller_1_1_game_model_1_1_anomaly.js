var class_asteroid_dweller_1_1_game_model_1_1_anomaly =
[
    [ "Anomaly", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a0ed82b4323860a44af54d74212622e6d", null ],
    [ "Anomaly", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#aff5ddb3bef850cb185f71e8e37fcc028", null ],
    [ "CollidesWith", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a9ceae327fdd2bc44349d7f9898ee4163", null ],
    [ "ResetInstance", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a5442c67a32c1d52f959f2f56b5a11704", null ],
    [ "CX", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a3997bf20db05fe14fe7fe0a7e4167167", null ],
    [ "CY", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a95639285f903d436d80bf14b26d1d34e", null ],
    [ "Layer", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#acc83b35a1aa40d835f85abce23fb09aa", null ],
    [ "Rotation", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#af7fc073f8b748116bfbd4a95b7260f4d", null ],
    [ "Shape", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html#a5046aa0fb309381277f3bb79718c5a49", null ]
];