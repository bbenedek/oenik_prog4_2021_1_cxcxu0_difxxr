var namespace_asteroid_dweller_1_1_game_model_interfaces =
[
    [ "IAnomaly", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_anomaly.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_anomaly" ],
    [ "IAsteroid", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_asteroid.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_asteroid" ],
    [ "IGameModel", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model" ],
    [ "IGameObject", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_object.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_object" ],
    [ "ILander", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_lander.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_lander" ],
    [ "IPlatform", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_platform.html", null ],
    [ "IPlayer", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_player.html", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_player" ]
];