var dir_8c96e1d945be46e7acaf629a7bdc0a2d =
[
    [ "AsteroidDweller.DwellerControl", "dir_b5b4e2d68ba28ec9e6dcbe33bfd12a3a.html", "dir_b5b4e2d68ba28ec9e6dcbe33bfd12a3a" ],
    [ "AsteroidDweller.GameModel", "dir_4f6f14c3408e03c5e5c3d4a3b725ae00.html", "dir_4f6f14c3408e03c5e5c3d4a3b725ae00" ],
    [ "AsteroidDweller.Interfaces", "dir_3fef778e40dcb1948e035aab9459823d.html", "dir_3fef778e40dcb1948e035aab9459823d" ],
    [ "AsteroidDweller.Logic", "dir_3e5db2b86d6d84526ad693ae121f49fe.html", "dir_3e5db2b86d6d84526ad693ae121f49fe" ],
    [ "AsteroidDweller.LogicInterface", "dir_515c4aec4dcabcaa8a13fe5b67cd8e5c.html", "dir_515c4aec4dcabcaa8a13fe5b67cd8e5c" ],
    [ "AsteroidDweller.LogicTests", "dir_60f7745d93b27cdeda08fd79da3e6e95.html", "dir_60f7745d93b27cdeda08fd79da3e6e95" ],
    [ "AsteroidDweller.ModelInterface", "dir_9b5811376413f399877f7f284cd4d53b.html", "dir_9b5811376413f399877f7f284cd4d53b" ],
    [ "AsteroidDweller.StorageRepository", "dir_b776a7ce4e12999f1ddeb7258cd919c4.html", "dir_b776a7ce4e12999f1ddeb7258cd919c4" ],
    [ "AsteroidDweller.Test", "dir_1eddf89deac14d9a6e4cac78cab03a5d.html", "dir_1eddf89deac14d9a6e4cac78cab03a5d" ],
    [ "AsteroidDweller.UI", "dir_d9b36b9855ffd65e68b8d0877dd2923f.html", "dir_d9b36b9855ffd65e68b8d0877dd2923f" ],
    [ "WpfLibrary1", "dir_4c353b5bef86ff9d243553d31f2a02d8.html", "dir_4c353b5bef86ff9d243553d31f2a02d8" ],
    [ "IGameModel.cs", "_i_game_model_8cs_source.html", null ]
];