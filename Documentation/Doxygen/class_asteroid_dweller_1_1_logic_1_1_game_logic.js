var class_asteroid_dweller_1_1_logic_1_1_game_logic =
[
    [ "GameLogic", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a5d9582f06536a05933a408472925b9b8", null ],
    [ "AsteroidTic", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a367eafbcf781ca6bb90dca3dd35d7ddf", null ],
    [ "DoTurn", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#ac69b26e89ba2f3d6e648f6030d68a89b", null ],
    [ "GameOver", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#ae49d101becc7c769ae601d3a6ce66086", null ],
    [ "LanderBoost", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a95cadd7389c2352c1571bbfc8aa307e0", null ],
    [ "LanderTick", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#afe14509c4e37f19821109126e7ba5eb4", null ],
    [ "LoadGame", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a43ffffea9c20778ac9b4df50ad991697", null ],
    [ "NewAnomaly", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a2508bec27f6b1fcb3e7460fe1f9031fd", null ],
    [ "NewAsteroid", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a572d678f41c3f0142f569e5285f22006", null ],
    [ "SaveGame", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a51f182fbc8183a5d0b1d186821b47a66", null ],
    [ "UpdateToplist", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a907e3c82995dbfeb45fcb35d37f80c5e", null ]
];