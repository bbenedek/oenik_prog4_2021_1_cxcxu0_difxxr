var searchData=
[
  ['name_50',['Name',['../class_asteroid_dweller_1_1_game_model_1_1_player.html#a17ab882faf1331bdafe8564926d8cba7',1,'AsteroidDweller.GameModel.Player.Name()'],['../interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_player.html#aebd4462b3ee35b5e0f49a1573e1727ea',1,'AsteroidDweller.GameModelInterfaces.IPlayer.Name()']]],
  ['newanomaly_51',['NewAnomaly',['../class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a2508bec27f6b1fcb3e7460fe1f9031fd',1,'AsteroidDweller.Logic.GameLogic.NewAnomaly()'],['../interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#a646398066a8dbbe369d399817835821b',1,'AsteroidDweller.GameLogicInterfaces.IGameLogic.NewAnomaly()']]],
  ['newasteroid_52',['NewAsteroid',['../class_asteroid_dweller_1_1_logic_1_1_game_logic.html#a572d678f41c3f0142f569e5285f22006',1,'AsteroidDweller.Logic.GameLogic.NewAsteroid()'],['../interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html#abfeff54de24f377b5adab763fa3f90d6',1,'AsteroidDweller.GameLogicInterfaces.IGameLogic.NewAsteroid()']]]
];
