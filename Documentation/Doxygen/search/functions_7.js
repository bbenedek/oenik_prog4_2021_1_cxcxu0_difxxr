var searchData=
[
  ['platform_131',['Platform',['../class_asteroid_dweller_1_1_game_model_1_1_platform.html#a6ac067f2ed4866d1fde27213d8c00114',1,'AsteroidDweller.GameModel.Platform.Platform()'],['../class_asteroid_dweller_1_1_game_model_1_1_platform.html#a5ed7654a138c2db0f76079a87478beec',1,'AsteroidDweller.GameModel.Platform.Platform(double newcx, double newcy)']]],
  ['player_132',['Player',['../class_asteroid_dweller_1_1_game_model_1_1_player.html#ab18155c9ee1b14e23384d31b1618b706',1,'AsteroidDweller.GameModel.Player.Player(string name)'],['../class_asteroid_dweller_1_1_game_model_1_1_player.html#a398d5fb6bd4f60bca5cac13a0e954189',1,'AsteroidDweller.GameModel.Player.Player(string name, int score)'],['../class_asteroid_dweller_1_1_game_model_1_1_player.html#a92fa7da8e7c995054dc04729b9d11b02',1,'AsteroidDweller.GameModel.Player.Player()']]]
];
