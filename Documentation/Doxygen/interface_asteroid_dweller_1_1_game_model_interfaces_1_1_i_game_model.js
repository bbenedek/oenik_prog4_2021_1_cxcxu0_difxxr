var interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model =
[
    [ "Anomaly", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a6a97dcefbb8ab38c452a71d625596dc8", null ],
    [ "Asteroid", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a89389d821182a10056aae3bbdd6b3bf1", null ],
    [ "Asteroids", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a410350b64653414896fc10d0c67c22a4", null ],
    [ "Lander", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a401e8850a2676dac7e630cb47d085bfe", null ],
    [ "Platform", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a200497f6254493e2d52526c40567f0ca", null ],
    [ "Player", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a8b0f751220b558c557a3df015a12fd29", null ],
    [ "WindowHeight", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#a4079d8459b560053a028df14c07fa1f3", null ],
    [ "WindowWidth", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html#ae8356252dfc8ec0a3003358ca033c57e", null ]
];