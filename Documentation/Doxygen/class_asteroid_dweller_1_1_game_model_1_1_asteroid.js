var class_asteroid_dweller_1_1_game_model_1_1_asteroid =
[
    [ "Asteroid", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#af5d36002615f409224bae63a46b14e07", null ],
    [ "Asteroid", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#ad1e5c9c4a241683d4aaa804a36174e8e", null ],
    [ "CollidesWith", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#a230c4053f8f15ba272e01166a589d5d2", null ],
    [ "ResetInstance", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#afb945496f35789e31dfb38ec34a0f76f", null ],
    [ "CX", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#ae135429c5806c17f0eeb6d9ea4a4b1f4", null ],
    [ "CY", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#aa032f6ffbfd75026f26102ec6335eb53", null ],
    [ "Layer", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#a46b9c59e77ea745442d4df0f586ca881", null ],
    [ "Rotation", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#aa3b009ec84cd4b3a5e9b2730fe2e3013", null ],
    [ "Shape", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html#a100e13c4756fe4076e06d81045347069", null ]
];