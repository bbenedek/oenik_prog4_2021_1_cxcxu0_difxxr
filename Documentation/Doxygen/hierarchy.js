var hierarchy =
[
    [ "Application", null, [
      [ "AsteroidDweller::UI::App", "class_asteroid_dweller_1_1_u_i_1_1_app.html", null ],
      [ "AsteroidDweller::UI::App", "class_asteroid_dweller_1_1_u_i_1_1_app.html", null ],
      [ "AsteroidDweller::UI::App", "class_asteroid_dweller_1_1_u_i_1_1_app.html", null ],
      [ "AsteroidDweller::UI::App", "class_asteroid_dweller_1_1_u_i_1_1_app.html", null ],
      [ "AsteroidDweller::UI::App", "class_asteroid_dweller_1_1_u_i_1_1_app.html", null ]
    ] ],
    [ "WpfLibrary1.Class1", "class_wpf_library1_1_1_class1.html", null ],
    [ "Class1", "class_class1.html", null ],
    [ "FrameworkElement", null, [
      [ "AsteroidDweller.DwellerControl.AsteroidDwellerControl", "class_asteroid_dweller_1_1_dweller_control_1_1_asteroid_dweller_control.html", null ]
    ] ],
    [ "AsteroidDweller.DwellerControl.HelperSetter", "class_asteroid_dweller_1_1_dweller_control_1_1_helper_setter.html", null ],
    [ "IComponentConnector", null, [
      [ "AsteroidDweller::UI::GameWindow", "class_asteroid_dweller_1_1_u_i_1_1_game_window.html", null ],
      [ "AsteroidDweller::UI::GameWindow", "class_asteroid_dweller_1_1_u_i_1_1_game_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ]
    ] ],
    [ "AsteroidDweller.GameLogicInterfaces.IGameLogic", "interface_asteroid_dweller_1_1_game_logic_interfaces_1_1_i_game_logic.html", [
      [ "AsteroidDweller.Logic.GameLogic", "class_asteroid_dweller_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "AsteroidDweller.GameModelInterfaces.IGameModel", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_model.html", [
      [ "AsteroidDweller.GameModel.GameModel", "class_asteroid_dweller_1_1_game_model_1_1_game_model.html", null ]
    ] ],
    [ "AsteroidDweller.GameModelInterfaces.IGameObject", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_game_object.html", [
      [ "AsteroidDweller.GameModel.GameObject", "class_asteroid_dweller_1_1_game_model_1_1_game_object.html", null ],
      [ "AsteroidDweller.GameModelInterfaces.IAnomaly", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_anomaly.html", [
        [ "AsteroidDweller.GameModel.Anomaly", "class_asteroid_dweller_1_1_game_model_1_1_anomaly.html", null ]
      ] ],
      [ "AsteroidDweller.GameModelInterfaces.IAsteroid", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_asteroid.html", [
        [ "AsteroidDweller.GameModel.Asteroid", "class_asteroid_dweller_1_1_game_model_1_1_asteroid.html", null ]
      ] ],
      [ "AsteroidDweller.GameModelInterfaces.ILander", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_lander.html", [
        [ "AsteroidDweller.GameModel.Lander", "class_asteroid_dweller_1_1_game_model_1_1_lander.html", null ]
      ] ],
      [ "AsteroidDweller.GameModelInterfaces.IPlatform", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_platform.html", [
        [ "AsteroidDweller.GameModel.Platform", "class_asteroid_dweller_1_1_game_model_1_1_platform.html", null ]
      ] ]
    ] ],
    [ "AsteroidDweller.GameModelInterfaces.IPlayer", "interface_asteroid_dweller_1_1_game_model_interfaces_1_1_i_player.html", [
      [ "AsteroidDweller.GameModel.Player", "class_asteroid_dweller_1_1_game_model_1_1_player.html", null ]
    ] ],
    [ "AsteroidDweller.StorageInterfaces.IStorageRepository", "interface_asteroid_dweller_1_1_storage_interfaces_1_1_i_storage_repository.html", [
      [ "AsteroidDweller.StorageRepository.DStorageRepository", "class_asteroid_dweller_1_1_storage_repository_1_1_d_storage_repository.html", null ]
    ] ],
    [ "AsteroidDweller.Test.LogicTests", "class_asteroid_dweller_1_1_test_1_1_logic_tests.html", null ],
    [ "AsteroidDweller.DwellerControl.Renderer", "class_asteroid_dweller_1_1_dweller_control_1_1_renderer.html", null ],
    [ "Window", null, [
      [ "AsteroidDweller::UI::GameWindow", "class_asteroid_dweller_1_1_u_i_1_1_game_window.html", null ],
      [ "AsteroidDweller::UI::GameWindow", "class_asteroid_dweller_1_1_u_i_1_1_game_window.html", null ],
      [ "AsteroidDweller::UI::GameWindow", "class_asteroid_dweller_1_1_u_i_1_1_game_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ],
      [ "AsteroidDweller::UI::MainWindow", "class_asteroid_dweller_1_1_u_i_1_1_main_window.html", null ]
    ] ]
];