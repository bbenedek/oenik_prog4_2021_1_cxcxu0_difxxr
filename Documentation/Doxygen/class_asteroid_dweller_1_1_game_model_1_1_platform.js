var class_asteroid_dweller_1_1_game_model_1_1_platform =
[
    [ "Platform", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#a6ac067f2ed4866d1fde27213d8c00114", null ],
    [ "Platform", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#a5ed7654a138c2db0f76079a87478beec", null ],
    [ "CollidesWith", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#a5a21bb63dc307facf1bdfb4281779d86", null ],
    [ "CX", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#ad11a61fa0e495f2b2115595cc8a8b058", null ],
    [ "CY", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#a0032995732b529d12fbd57cba90e219d", null ],
    [ "Layer", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#a664c78a4446a11020f3e86b45ce2dc4f", null ],
    [ "Rotation", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#aa4836dd9f67720b0264148fb174e46d8", null ],
    [ "Shape", "class_asteroid_dweller_1_1_game_model_1_1_platform.html#aa8eab9405e2948c44dbf51b34ba6ce0e", null ]
];