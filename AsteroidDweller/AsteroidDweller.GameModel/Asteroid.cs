﻿// <copyright file="Asteroid.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Class represents the bad asteroid.
    /// </summary>
    public class Asteroid : IAsteroid
    {
        /// <summary>
        /// Stores the details of the shape.
        /// </summary>
        private Geometry area;

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        private double rotDegree;

        /// <summary>
        /// Initializes a new instance of the <see cref="Asteroid"/> class.
        /// </summary>
        public Asteroid()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Asteroid"/> class.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public Asteroid(double newcx, double newcy, int layer)
        {
            this.Layer = layer;

            this.CX = newcx;
            this.CY = newcy;

            if (this.Layer == 1)
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 6, 6);
                this.area = asteroid;
            }
            else
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 3, 3);
                this.area = asteroid;
            }
        }

        /// <summary>
        /// Gets or sets the layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets the shape of the object.
        /// </summary>
        public Geometry Shape
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.rotDegree, this.CX, this.CY));

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        public double Rotation
        {
            get
            {
                return this.rotDegree * Math.PI / 180;
            }

            set
            {
                this.rotDegree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Collidation check.
        /// </summary>
        /// <param name="other">It waits an IgameObject.</param>
        /// <returns>REturns collidation check.</returns>
        public bool CollidesWith(IGameObject other)
        {
            return Geometry.Combine(
                this.Shape,
                other.Shape,
                GeometryCombineMode.Intersect,
                null).GetArea() > 0;
        }

        /// <summary>
        /// Method for reseting the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public void ResetInstance(double newcx, double newcy, int layer)
        {
            this.Layer = layer;

            this.CX = newcx;
            this.CY = newcy;

            if (this.Layer == 1)
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 6, 6);
                this.area = asteroid;
            }
            else
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 3, 3);
                this.area = asteroid;
            }
        }
    }
}
