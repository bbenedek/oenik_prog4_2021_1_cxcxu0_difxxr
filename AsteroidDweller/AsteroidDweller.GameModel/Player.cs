﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Contains the player score and name.
    /// </summary>
    public class Player : IPlayer
    {
        /// <summary>
        /// The name of the player.
        /// </summary>
        private string name;

        /// <summary>
        /// The score of the player.
        /// </summary>
        private int score;

        /// <summary>
        /// Gets or sets the score of the player.
        /// </summary>
        public int Score { get => this.score; set => this.score = value; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get => this.name; set => this.name = value; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// Sets the starter name.
        /// </summary>
        /// <param name="name">It waits a name.</param>
        public Player(string name)
        {
            this.Name = name;
            this.Score = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// The other ctor is adjust the name, score.
        /// </summary>
        /// <param name="name">the name paameret.</param>
        /// <param name="score">the name oparameter.</param>
        public Player(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// Constructor for the player class.
        /// </summary>
        public Player()
        {
        }
    }
}
