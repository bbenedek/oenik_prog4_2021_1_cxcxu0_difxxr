﻿// <copyright file="Anomaly.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System.Windows;
    using System.Windows.Media;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Class repsrents a new instance of the Asteroid.
    /// </summary>
    public class Anomaly : IAnomaly
    {
        /// <summary>
        /// Geometry for storing the shape's details.
        /// </summary>
        private Geometry area;

        /// <summary>
        /// Gets or sets rotation.
        /// </summary>
        private double rotDegree = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="Anomaly"/> class.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// /// <param name="layer">The layer of the anomaly.</param>
        public Anomaly(double newcx, double newcy, int layer)
        {
            this.Layer = layer;

            this.CX = newcx;
            this.CY = newcy;

            if (this.Layer == 1)
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 4.4, 4.4);
                this.area = asteroid;
            }
            else
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 2.8, 2.8);
                this.area = asteroid;
            }
        }

        /// <summary>
        /// Gets or sets the Layer of the object.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets the shap of the object.
        /// </summary>
        public Geometry Shape
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.rotDegree, this.CX, this.CY));

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets the rotation od the object.
        /// </summary>
        public double Rotation { get; set; }

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Anomaly"/> class.
        /// Sets the anomaly layer.
        /// </summary>
        /// <param name="layer">It waits an int layer.</param>
        public Anomaly(int layer)
        {
            this.Layer = layer;
        }

        /// <summary>
        /// Method for testing area intersection.
        /// </summary>
        /// <param name="other">Other shape.</param>
        /// <returns>True if intersects.</returns>
        public bool CollidesWith(IGameObject other)
        {
            return Geometry.Combine(
                this.Shape,
                other.Shape,
                GeometryCombineMode.Intersect,
                null).GetArea() > 0;
        }

        /// <summary>
        /// Method for reseting the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public void ResetInstance(double newcx, double newcy, int layer)
        {
            this.Layer = layer;

            this.CX = newcx;
            this.CY = newcy;

            if (this.Layer == 1)
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 4.4, 4.4);
                this.area = asteroid;
            }
            else
            {
                EllipseGeometry asteroid = new EllipseGeometry(new Point(0, 0), 2.8, 2.8);
                this.area = asteroid;
            }
        }
    }
}
