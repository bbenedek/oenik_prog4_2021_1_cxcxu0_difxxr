﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "CS0649", Justification = "<NikGitStats>", Scope = "module")]

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.GameModell.#ctor(System.Int32,System.Int32)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.GameObject.#ctor(System.Int32)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.GameObject.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Player.#ctor(System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Anomaly.#ctor(System.Int32)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "CA1014:Mark assemblies with CLSCompliant", Justification = "Not neeeded in this project.")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not needed in this project.", Scope = "member", Target = "~P:AsteroidDweller.GameModel.GameModell.Asteroids")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.GameModell.#ctor(System.Double,System.Double)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Platform.CollidesWith(AsteroidDweller.GameModelInterfaces.IGameObject)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Lander.CollidesWith(AsteroidDweller.GameModelInterfaces.IGameObject)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.GameObject.CollidesWith(AsteroidDweller.GameModelInterfaces.IGameObject)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1805:Do not initialize unnecessarily", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.rotDegree")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.rotDegree")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.rotDegree")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.rotDegree")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Anomaly.CollidesWith(AsteroidDweller.GameModelInterfaces.IGameObject)~System.Boolean")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "Not needed in this project.")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not needed in this project.", Scope = "member", Target = "~M:AsteroidDweller.GameModel.Asteroid.CollidesWith(AsteroidDweller.GameModelInterfaces.IGameObject)~System.Boolean")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.Anomaly.rotDegree")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.area")]
[assembly: SuppressMessage("Performance", "CA1805:Do not initialize unnecessarily", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.Anomaly.rotDegree")]
[assembly: SuppressMessage("Performance", "CA1805:Do not initialize unnecessarily", Justification = "Not needed in this project.", Scope = "member", Target = "~F:AsteroidDweller.GameModel.GameObject.area")]
