// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System;
    using System.Windows.Media;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Ancestor of the game objects.
    /// </summary>
    public class GameObject : IGameObject
    {
        /// <summary>
        /// Geometry for storing the shapes details.
        /// </summary>
        private Geometry area = null;

        /// <summary>
        /// Stores the rotation degree.
        /// </summary>
        private double rotDegree;

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets  or sets the layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        public double Rotation
        {
            get
            {
                return this.rotDegree * Math.PI / 180;
            }

            set
            {
                this.rotDegree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Gets the shape of visual.
        /// </summary>
        public Geometry Shape
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.rotDegree, this.CX, this.CY));

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Detects if 2 objects are overlapping with each other, will be used for the Lander.
        /// </summary>
        /// <param name="other">The visual to combine with.</param>
        /// <returns>Returns the combined area, if > 0, then the objects have collided.</returns>
        public bool CollidesWith(IGameObject other)
        {
            return Geometry.Combine(this.Shape, other.Shape, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// Contstructor of the class.
        /// </summary>
        /// <param name="layer">int layer.</param>
        public GameObject(int layer)
        {
            this.Layer = layer;
        }
    }
}
