﻿// <copyright file="Lander.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Class represents the space ship.
    /// </summary>
    public class Lander : ILander
    {
        /// <summary>
        /// Geometry for storing the shapes details.
        /// </summary>
        private Geometry area;

        /// <summary>
        /// Stores the rotation degree.
        /// </summary>
        private double rotDegree;

        /// <summary>
        /// Initializes a new instance of the <see cref="Lander"/> class.
        /// Space ship entity.
        /// </summary>
        public Lander()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Lander"/> class.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        public Lander(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;

            this.Layer = 1;

            GeometryGroup group = new GeometryGroup();
            group.Children.Add(new EllipseGeometry(new Point(10, 10), 15, 5));
            group.Children.Add(new EllipseGeometry(new Point(6, 10), 2, 9));
            group.Children.Add(new EllipseGeometry(new Point(14, 14), 2, 16));
            this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
        }

        /// <summary>
        /// Gets or sets koordinate dx.
        /// </summary>
        public double DX { get; set; }

        /// <summary>
        /// Gets or sets coordinate dy.
        /// </summary>
        public double DY { get; set; }

        /// <summary>
        /// Gets or sets layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets shape.
        /// </summary>
        public Geometry Shape
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.rotDegree, this.CX, this.CY));

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        public double Rotation
        {
            get
            {
                return /*this.rotDegree * Math.PI / 180*/ 0;
            }

            set
            {
                this.rotDegree = /*180 * value / Math.PI */ 0;
            }
        }

        /// <summary>
        /// Collidation check.
        /// </summary>
        /// <param name="other">It waits an IgameObject.</param>
        /// <returns>REturns collidation check.</returns>
        public bool CollidesWith(IGameObject other)
        {
            return Geometry.Combine(
                this.Shape,
                other.Shape,
                GeometryCombineMode.Intersect,
                null).GetArea() > 0;
        }

        /// <summary>
        /// Method for reseting the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public void ResetInstance(double newcx, double newcy, int layer)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.Layer = layer;

            if (this.Layer == 1)
            {
                this.DX = 2;
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 15, 5));
                group.Children.Add(new EllipseGeometry(new Point(14, 10), 2, 9));
                group.Children.Add(new EllipseGeometry(new Point(6, 14), 2, 16));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
            else if (this.Layer == 0)
            {
                this.DX = -2;
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 10.5, 3.5));
                group.Children.Add(new EllipseGeometry(new Point(14, 14), 1.4, 11.2));
                group.Children.Add(new EllipseGeometry(new Point(6, 10), 1.4, 6.3));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
        }

        /// <summary>
        /// Method for rotating the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        public void RotateInstance(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;

            if (this.Layer == 1 && this.DX > 0)
            {
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 15, 5));
                group.Children.Add(new EllipseGeometry(new Point(6, 10), 2, 9));
                group.Children.Add(new EllipseGeometry(new Point(14, 14), 2, 16));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
            else if (this.Layer == 1 && this.DX <= 0)
            {
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 15, 5));
                group.Children.Add(new EllipseGeometry(new Point(14, 10), 2, 9));
                group.Children.Add(new EllipseGeometry(new Point(6, 14), 2, 16));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
            else if (this.Layer == 0 && this.DX < 0)
            {
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 10.5, 3.5));
                group.Children.Add(new EllipseGeometry(new Point(6, 14), 1.4, 11.2));
                group.Children.Add(new EllipseGeometry(new Point(14, 10), 1.4, 6.3));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
            else if (this.Layer == 0 && this.DX >= 0)
            {
                GeometryGroup group = new GeometryGroup();
                group.Children.Add(new EllipseGeometry(new Point(10, 10), 10.5, 3.5));
                group.Children.Add(new EllipseGeometry(new Point(14, 14), 1.4, 11.2));
                group.Children.Add(new EllipseGeometry(new Point(6, 10), 1.4, 6.3));
                this.area = group.GetWidenedPathGeometry(new Pen(Brushes.Black, 2));
            }
        }
    }
}
