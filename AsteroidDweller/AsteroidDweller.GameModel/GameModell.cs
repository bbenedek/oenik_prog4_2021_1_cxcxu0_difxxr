﻿// <copyright file="GameModell.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System.Collections.Generic;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Model for the base objects.
    /// </summary>
    public class GameModell : IGameModel
    {
        /// <summary>
        /// Gets or sets the anomaly.
        /// </summary>
        public IAnomaly Anomaly { get; set; }

        /// <summary>
        /// Gets or sets the assteroids.
        /// </summary>
        public List<IAsteroid> Asteroids { get; set; }

        /// <summary>
        /// Gets or sets the asteroid.
        /// </summary>
        public IAsteroid Asteroid { get; set; }

        /// <summary>
        /// Gets or sets the lander.
        /// </summary>
        public ILander Lander { get; set; }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        public IPlatform Platform { get; set; }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        public IPlayer Player { get; set; }

        /// <summary>
        /// Gets or sets the windowWindth.
        /// </summary>
        public double WindowWidth { get; set; }

        /// <summary>
        /// Gets or sets the windowHeight.
        /// </summary>
        public double WindowHeight { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModell"/> class.
        /// Sets the Model window height/width.
        /// </summary>
        /// <param name="wWidth">the witdht of window.</param>
        /// <param name="wHeight">the heght of the windiw.</param>
        public GameModell(double wWidth, double wHeight)
        {
            this.WindowHeight = wHeight;
            this.WindowWidth = wWidth;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModell"/> class.
        /// </summary>
        public GameModell()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModell"/> class.
        /// </summary>
        /// <param name="wWidth">width.</param>
        /// <param name="wHeight">height.</param>
        /// <param name="playerName">playerName.</param>
        public GameModell(double wWidth, double wHeight, string playerName)
        {
            this.WindowHeight = wHeight;
            this.WindowWidth = wWidth;
            this.Player = new Player();
            this.Player.Name = playerName;
            this.Player.Score = 0;
        }
    }
}
