﻿// <copyright file="Platform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModel
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Represents the starting and finishing platform.
    /// </summary>
    public class Platform : IPlatform
    {
        /// <summary>
        /// Geometry for storing the shapes details.
        /// </summary>
        private Geometry area;

        /// <summary>
        /// Stores the rotation degree.
        /// </summary>
        private double rotDegree;

        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        public Platform()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        public Platform(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.Layer = 1;

            RectangleGeometry platform =
                new RectangleGeometry(new Rect(0, 0, 30, 2));

            this.area = platform;
        }

        /// <summary>
        /// Gets or sets the layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets the shpe.
        /// </summary>
        public Geometry Shape
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.rotDegree, this.CX, this.CY));

                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        public double Rotation
        {
            get
            {
                return this.rotDegree * Math.PI / 180;
            }

            set
            {
                this.rotDegree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Collidation check.
        /// </summary>
        /// <param name="other">It waits an IgameObject.</param>
        /// <returns>REturns collidation check.</returns>
        public bool CollidesWith(IGameObject other)
        {
            return Geometry.Combine(
                this.Shape,
                other.Shape,
                GeometryCombineMode.Intersect,
                null).GetArea() > 0;
        }
    }
}
