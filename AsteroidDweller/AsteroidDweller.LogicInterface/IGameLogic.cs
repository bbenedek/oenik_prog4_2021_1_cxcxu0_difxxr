﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameLogicInterfaces
{
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Interface for defining the GameLogic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Gets or sets a value indicating whether gets ors sets the boolean gameover.
        /// </summary>
        public bool GameIsOver { get; set; }

        /// <summary>
        /// Method for setting the Anomaly's new location.
        /// </summary>
        void NewAnomaly();

        /// <summary>
        /// Method for adding a new Asteroid to the asteroid list.
        /// </summary>
        void NewAsteroid();

        /// <summary>
        /// Sets the lander's horizontal and vertical accelaration based on user input.
        /// </summary>
        /// <param name="deltaX">Horizontal acceleration.</param>
        /// <param name="deltaY">Vertical acceleration.</param>
        void LanderBoost(int deltaX, int deltaY);

        /// <summary>
        /// Method for calculating the new position of the Asteroids.
        /// </summary>
        /// <param name="asteroid">One Asteroid from the Asteroid list.</param>
        void AsteroidTic(IAsteroid asteroid);

        /// <summary>
        /// Method for calculating the new position and orientation of the Lander.
        /// </summary>
        void LanderTick();

        /// <summary>
        /// Method for calling a game turn, where objects move and possibly collide.
        /// </summary>
        void DoTurn();

        /// <summary>
        /// Initializes a new instance of the GameLogic class, based on a saved game.
        /// </summary>
        void LoadGame();

        /// <summary>
        /// Saves the current GameModel instance details.
        /// </summary>
        void SaveGame();

        /// <summary>
        /// Saves the player's name an score to the toplist.
        /// </summary>
        void UpdateToplist();

        /// <summary>
        /// Ends the current game session, and calls the UpdateToplist(IPlayer player).
        /// </summary>
        void GameOver();
    }
}
