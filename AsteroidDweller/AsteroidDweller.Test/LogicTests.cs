﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.Test
{
    using AsteroidDweller.GameModel;
    using AsteroidDweller.Logic;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the gameLogic methods.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private GameModell gameModel;

        /// <summary>
        /// Tests the gameLogic's methods.
        /// </summary>
        [Test]
        public void TestGameLogicInstance()
        {
            // Arrange
            GameModell gameModel = new GameModell(200, 200);

            // Act
            var logic = new GameLogic(gameModel);

            // Assert/Verify
            Assert.That(gameModel.WindowHeight, Is.EqualTo(200));

            Assert.That(this.gameModel.Platform.Layer, Is.EqualTo(1));
            Assert.That(this.gameModel.Lander.Layer, Is.EqualTo(1));
            Assert.That(this.gameModel.Anomaly.CX, Is.GreaterThanOrEqualTo(50));
        }

        /// <summary>
        /// Tests the gameLogic's NewAsteroid() method.
        /// </summary>
        [Test]
        public void TestGameLogicNewAnomaly()
        {
            // Arrange
            this.gameModel = new GameModell(150, 150);

            // Act
            var logic = new GameLogic(this.gameModel);
            logic.NewAnomaly();

            // Assert/Verify
            Assert.That(this.gameModel.Anomaly.CY, Is.LessThan(100));
            Assert.That(this.gameModel.Anomaly.CX, Is.LessThan(100));
            Assert.That(this.gameModel.Player.Score, Is.LessThan(1));
        }

        /// <summary>
        /// Tests the gameLogic's NewAsteroid() method.
        /// </summary>
        [Test]
        public void TestGameLogicNewAsteroid()
        {
            // Arrange
            this.gameModel = new GameModell(100, 100);

            // Act
            var logic = new GameLogic(this.gameModel);
            logic.NewAsteroid();

            // Assert/Verify
            Assert.That(this.gameModel.Asteroids.Count, Is.EqualTo(1));
            Assert.That(this.gameModel.Asteroids[0].CY, Is.LessThan(80));
            Assert.That(this.gameModel.Asteroids[0].CX, Is.EqualTo(5));
        }

        /// <summary>
        /// Tests the gameLogic's LanderTick() and Boost() method.
        /// </summary>
        [Test]
        public void TestGameLogicLanderTick()
        {
            // Arrange
            this.gameModel = new GameModell(100, 100);

            // Act
            var logic = new GameLogic(this.gameModel);
            logic.LanderBoost(35, 0);
            logic.LanderTick();

            // Assert/Verify
            Assert.That(this.gameModel.Lander.CX, Is.EqualTo(50));
            Assert.That(this.gameModel.Lander.CY, Is.EqualTo(50));
        }

        /// <summary>
        /// Tests the gameLogic's AsteroidTick()method.
        /// </summary>
        [Test]
        public void TestGameLogicAsteroidTick()
        {
            // Arrange
            this.gameModel = new GameModell(100, 100);

            // Act
            var logic = new GameLogic(this.gameModel);
            logic.NewAsteroid();
            var testAsteroid = this.gameModel.Asteroids[0];
            logic.AsteroidTic(testAsteroid);

            // Assert/Verify
            Assert.That(testAsteroid.CX, Is.EqualTo(15));
        }

        /// <summary>
        /// Tests the collide method for lander and asteroid.
        /// </summary>
        [Test]
        public void TestAsteroidCollide()
        {
            // Arrange
            this.gameModel = new GameModell(100, 100);

            // Act
            var logic = new GameLogic(this.gameModel);
            logic.NewAsteroid();

            var testAsteroid = this.gameModel.Asteroids[0];

            testAsteroid.CX = 50;
            testAsteroid.CY = 50;
            testAsteroid.Layer = 1;

            this.gameModel.Lander.Layer = 1;
            logic.LanderBoost(35, 0);
            logic.LanderTick();

            // Assert/Verify
            Assert.That(this.gameModel.Lander.CX, Is.EqualTo(50));
            Assert.That(this.gameModel.Lander.CY, Is.EqualTo(50));
            Assert.That(testAsteroid.CX, Is.EqualTo(50));
            Assert.That(testAsteroid.CY, Is.EqualTo(50));
            Assert.That(testAsteroid.CollidesWith(this.gameModel.Lander), Is.False);
        }
    }
}
