﻿// <copyright file="HelperSetter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.DwellerControl
{
    /// <summary>
    /// Set the Lander position, speed depend on key action.
    /// </summary>
    public static class HelperSetter
    {
        /// <summary>
        /// Set the lander speed on Y.
        /// </summary>
        public const int UpperThrusters = 1;

        /// <summary>
        /// Set the lander speed on Y.
        /// </summary>
        public const int LowerThrusters = -5;

        /// <summary>
        /// Set the lander speed on X.
        /// </summary>
        public const int RightThrusters = -2;

        /// <summary>
        /// Set the lander speed on X.
        /// </summary>
        public const int LeftThrusters = 2;

        /// <summary>
        /// Set  the background.
        /// </summary>
        public const string BackGround = "../../../../AsteroidDweller.UI/Images/background.png";
    }
}
