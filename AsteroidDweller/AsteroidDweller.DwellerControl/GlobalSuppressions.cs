﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "CS0618", Justification = "<NikGitStats>", Scope = "module")]

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.DrawText(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1117:Parameters should be on same line or separate lines", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.DrawText(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1116:Split parameters should start on line after declaration", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.DrawText(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.DisplayBuilder(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not in scope.", Scope = "member", Target = "~F:AsteroidDweller.DwellerControl.HelperSetter.BackGround")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.AsteroidDwellerControl.OnRender(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "Not in scope.")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not in scope.", Scope = "member", Target = "~F:AsteroidDweller.DwellerControl.HelperSetter.UpperThrusters")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not in scope.", Scope = "member", Target = "~F:AsteroidDweller.DwellerControl.HelperSetter.RightThrusters")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not in scope.", Scope = "member", Target = "~F:AsteroidDweller.DwellerControl.HelperSetter.LeftThrusters")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Not in scope.", Scope = "member", Target = "~F:AsteroidDweller.DwellerControl.HelperSetter.LowerThrusters")]
[assembly: SuppressMessage("Style", "IDE0090:Use 'new(...)'", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.DrawText(System.Windows.Media.DrawingContext)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.#ctor(AsteroidDweller.GameModel.GameModell)")]
[assembly: SuppressMessage("Design", "CS0618:Validate arguments of public methods", Justification = "Not in scope.", Scope = "member", Target = "~M:AsteroidDweller.DwellerControl.Renderer.#ctor(AsteroidDweller.GameModel.GameModell)")]