// <copyright file="AsteroidDwellerControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.DwellerControl
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using AsteroidDweller.GameModel;
    using AsteroidDweller.Logic;

    /// <summary>
    /// The Control class for the game.
    /// </summary>
    public class AsteroidDwellerControl : FrameworkElement
    {
        private GameModell model;
        private GameLogic logic;
        private DispatcherTimer mainTimer;
        private Renderer renderer;
        private Window wndw;
        private string playerName = "add player name...";

        /// <summary>
        /// Initializes a new instance of the <see cref="AsteroidDwellerControl"/> class.
        /// </summary>
        public AsteroidDwellerControl()
        {
            this.Loaded += this.DwellerControl_Loaded;

            // this.KeyDown += this.DwellerControl_KeyDown;
        }

        /// <summary>
        /// Gets or Sets the player name.
        /// </summary>
        public string PlayerName
        {
            get => this.playerName;
            set => this.playerName = value;
        }

        /// <summary>
        /// Loads the game.
        /// </summary>
        public void LoadGame()
        {
            this.model = new GameModell(this.ActualWidth, this.ActualHeight);
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);
            this.logic.LoadGame();
            this.InvalidateVisual();
        }

        /// <summary>
        /// Shows the game toplist.
        /// </summary>
        public void Toplist()
        {
            this.model = new GameModell(this.ActualWidth, this.ActualHeight);
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);
            this.logic.UpdateToplist();
            this.InvalidateVisual();
        }

        /// <summary>
        /// Lists the toplist of the players.
        /// </summary>
        /// <returns>players name, scores.</returns>
        public string ToplistToShow()
        {
            this.model = new GameModell(this.ActualWidth, this.ActualHeight);
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);

            return this.logic.Toplist();
        }

        /// <summary>
        /// Method for saving the current game.
        /// </summary>
        public void SaveGame()
        {
            this.model = new GameModell(this.ActualWidth, this.ActualHeight);
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);
            this.logic.SaveGame();
            this.InvalidateVisual();
        }

        private void DwellerControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModell(this.ActualWidth, this.ActualHeight);
            this.renderer = new Renderer(this.model);
            this.logic = new GameLogic(this.model);

            /*Binging fails after control were loaded, needs to be checked.*/
            this.logic.SetPlayer("Testplayer", 0);

            this.wndw = Window.GetWindow(this);

            if (this.wndw != null)
            {
                    this.wndw.KeyDown += this.Wndw_KeyDown;

                    this.mainTimer = new DispatcherTimer();
                    this.mainTimer.Interval = TimeSpan.FromMilliseconds(100);
                    this.mainTimer.Tick += this.MainTimer_Tick;
                    this.mainTimer.Start();
            }
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            this.logic.DoTurn();
            this.InvalidateVisual();

            if (this.logic.GameIsOver == true)
            {
                this.mainTimer.Stop();
                MessageBox.Show($"Gratulálok {this.model.Player.Name}!\nAz elért pontszámod: {this.model.Player.Score}.");
                this.wndw.Close();
            }
        }

        private void Wndw_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up: this.logic.LanderBoost(0, HelperSetter.UpperThrusters); break;
                case Key.Down: this.logic.LanderBoost(0, HelperSetter.LowerThrusters); break;
                case Key.Space: this.mainTimer.IsEnabled = !this.mainTimer.IsEnabled; break;
                case Key.Right:
                    this.logic.LanderBoost(HelperSetter.RightThrusters, 0);
                    this.model.Lander.RotateInstance(this.model.Lander.CX, this.model.Lander.CY);
                    break;
                case Key.Left:
                    this.logic.LanderBoost(HelperSetter.LeftThrusters, 0);
                    this.model.Lander.RotateInstance(this.model.Lander.CX, this.model.Lander.CY);
                    break;
                case Key.Q:
                    if (this.model.Lander.DX > 5)
                    {
                        this.model.Lander.ResetInstance(this.model.Lander.CX, this.model.Lander.CY, 1);
                    }

                    break;

                case Key.E:
                    if (this.model.Lander.DX < -5)
                    {
                        this.model.Lander.ResetInstance(this.model.Lander.CX, this.model.Lander.CY, 0);
                    }

                    break;
            }
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.DisplayBuilder(drawingContext);
            }
        }
    }
}
