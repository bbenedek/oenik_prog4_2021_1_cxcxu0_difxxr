﻿// <copyright file="Renderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.DwellerControl
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using AsteroidDweller.GameModel;
    using AsteroidDweller.GameModelInterfaces;

    /// <summary>
    /// Renderer for the game outfit.
    /// </summary>
    public class Renderer
    {
        private GameModell model;
        private Typeface font = new Typeface("Arial");
        private Point textLocation = new Point(13, 13);
        private Rect backGrnd;
        private Brush backGround = new ImageBrush(new BitmapImage(new Uri(HelperSetter.BackGround, UriKind.Relative)));
        private Pen coral = new Pen(Brushes.Coral, 1);
        private Pen yellow = new Pen(Brushes.Yellow, 1);
        private Pen aquamarine = new Pen(Brushes.Aquamarine, 1);
        private Pen aliceBlue = new Pen(Brushes.AliceBlue, 1);
        private Pen black = new Pen(Brushes.Black, 1);
        private bool effect = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// Initialise a new instance of renderer.
        /// </summary>
        /// <param name="model">a gameModel.</param>
        public Renderer(GameModell model)
        {
            this.model = model;
            this.backGrnd = new Rect(0, 0, model.WindowWidth, model.WindowHeight);
        }

        /// <summary>
        /// Display the game.
        /// </summary>
        /// <param name="dctx">a Drawingcontext.</param>
        public void DisplayBuilder(DrawingContext dctx)
        {
            this.DrawBackGround(dctx);
            this.DrawText(dctx);
            this.DrawPlatform(dctx);
            this.DrawAsteroid(dctx);
            this.DrawAnomali(dctx);
            this.DrawLander(dctx);
        }

        private void DrawAsteroid(DrawingContext dctx)
        {
            foreach (IAsteroid asteroid in this.model.Asteroids)
            {
                dctx.DrawGeometry(Brushes.Black, this.black, asteroid.Shape);
            }
        }

        private void DrawAnomali(DrawingContext dctx)
        {
            if (this.effect == true)
            {
                dctx.DrawGeometry(Brushes.Aquamarine, this.aquamarine, this.model.Anomaly.Shape);
                this.effect = false;
            }
            else
            {
                dctx.DrawGeometry(Brushes.AliceBlue, this.aliceBlue, this.model.Anomaly.Shape);
                this.effect = true;
            }
        }

        private void DrawPlatform(DrawingContext dctx)
        {
                dctx.DrawGeometry(Brushes.Yellow, this.yellow, this.model.Platform.Shape);
        }

        private void DrawLander(DrawingContext dctx)
        {
            dctx.DrawGeometry(Brushes.Brown, this.coral, this.model.Lander.Shape);
        }

        // this.model.Player.Score.ToString()
        private void DrawText(DrawingContext dctx)
        {
            FormattedText text =

                 new FormattedText(this.model.Player.Score.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 13, Brushes.Black);

            dctx.DrawText(text, this.textLocation);
        }

        private void DrawBackGround(DrawingContext dctx)
        {
            dctx.DrawRectangle(this.backGround, null, new Rect(0, 0, this.model.WindowWidth, this.model.WindowHeight));
        }
    }
}
