﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "CA1014:Mark assemblies with CLSCompliant", Justification = "Not neeeded in this project.")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Not neeeded in this project.", Scope = "member", Target = "~M:AsteroidDweller.StorageInterfaces.IStorageRepository.TopList~System.Collections.Generic.List{AsteroidDweller.GameModelInterfaces.IPlayer}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Not neeeded in this project.", Scope = "member", Target = "~M:AsteroidDweller.StorageInterfaces.IStorageRepository.TopList~System.Collections.Generic.List{AsteroidDweller.GameModel.Player}")]
