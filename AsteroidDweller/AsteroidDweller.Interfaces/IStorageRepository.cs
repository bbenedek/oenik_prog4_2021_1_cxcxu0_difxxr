﻿// <copyright file="IStorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.StorageInterfaces
{
    using System.Collections.Generic;
    using AsteroidDweller.GameModel;

    /// <summary>
    /// Interface for describing the StorageRepository.
    /// </summary>
    public interface IStorageRepository
    {
        /// <summary>
        /// Method for saving the current game.
        /// </summary>
        /// <param name="savedGame">An instance holding the data of the saved game.</param>
        void SaveGame(GameModell savedGame);

        /// <summary>
        /// Method for saving the current player's score.
        /// </summary>
        /// <param name="player">An instance holding the data of the player.</param>
        void SaveTopList(Player player);

        /// <summary>
        /// Method for loading game from local file (csv, xml).
        /// </summary>
        /// <returns>An instance holding the data of the saved game.</returns>
        GameModell LastSave();

        /// <summary>
        /// Method for loading toplist from local file (csv, xml).
        /// </summary>
        /// <returns>Returns a List of Players (name and score).</returns>
        List<Player> TopList();
    }
}
