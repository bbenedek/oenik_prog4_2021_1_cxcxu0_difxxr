// <copyright file="DStorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.StorageRepository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using AsteroidDweller.GameModel;
    using AsteroidDweller.GameModelInterfaces;
    using AsteroidDweller.StorageInterfaces;

    /// <summary>
    /// Storing data.
    /// </summary>
    public class DStorageRepository : IStorageRepository
    {
        /// <summary>
        /// Save the game in xml file. Player name,score, Lander cx, cy, dx, dy, rotation, Anomaly cx,cy, ASteroids: cx,cy.
        /// </summary>
        /// <param name="savedGame">an interface in.</param>
        public void SaveGame(GameModell savedGame)
        {
            XDocument gameDetailsToXML = new XDocument(
                 new XDeclaration("1.0", "utf-8", "yes"),
                 new XElement(
                     "game",
                     new XElement("playerName", savedGame.Player.Name),
                     new XElement("playerScore", savedGame.Player.Score),
                     new XElement("landerCX", savedGame.Lander.CX),
                     new XElement("landerCY", savedGame.Lander.CY),
                     new XElement("landerDX", savedGame.Lander.DX),
                     new XElement("landerDY", savedGame.Lander.DY),
                     new XElement("landerRotation", savedGame.Lander.Rotation),
                     new XElement("anomalyCX", savedGame.Anomaly.CX),
                     new XElement("anomalyCY", savedGame.Anomaly.CY)));

            foreach (var item in savedGame.Asteroids)
            {
                XElement childElement = new XElement("asteroids");
                XAttribute attribute = new XAttribute("cord", item);
                childElement.Add(attribute);
                gameDetailsToXML.Add(childElement);
            }

            gameDetailsToXML.Save("savedGame.xml");
        }

        /// <summary>
        /// Saving the game details, save or extend the xml file.
        /// </summary>
        /// <param name="player">Waits an interface.</param>
        public void SaveTopList(Player player)
        {
            string filename = "playerDetails.xml";

            XDocument playerDetailsToXML = new XDocument();

            if (File.Exists(filename))
            {
                playerDetailsToXML = XDocument.Load(filename);
                playerDetailsToXML.Element("game").Add(new XElement("player", new XAttribute("score", player.Score), "bela"));

                // Sort(playerDetailsToXML);
            }
            else
            {
                playerDetailsToXML = new XDocument(
                 new XDeclaration("1.0", "utf-8", "yes"),
                 new XElement(
                     "game",
                     new XElement("player", new XAttribute("score", player.Score), "laci")));
            }

            playerDetailsToXML.Save(filename);
        }

        /// <summary>
        /// Loading the game status; https://stackoverflow.com/questions/43975677/c-sharp-parse-xml-into-object-with-child-elements.
        /// </summary>
        /// <returns>the game details.</returns>
        public GameModell LastSave()
        {
            GameModell gameResult = new GameModell();

            var details = XDocument.Load("savedGame.xml");
            var readXML = from akt in details.Descendants("game")
                          select new GameModell
                          {
                              WindowHeight = int.Parse(akt.Element("windowWidth").Value),
                              WindowWidth = int.Parse(akt.Element("windowWidth").Value),
                          };
            return gameResult;
        }

        /// <summary>
        /// Loading toplist from an existing xml file.
        /// </summary>
        /// <returns>details of the file in a list.</returns>
        public List<Player> TopList()
        {
            List<Player> result = new List<Player>();

            var details = XDocument.Load("playerDetails.xml");
            var gameD = details.Element("game");

            var readXML = from akt in details.Element("game").Descendants("player")
                          select akt;

            foreach (var item in readXML)
            {
                result.Add(new Player
                {
                    Score = int.Parse(item.Attribute("score").Value),
                    Name = item.Value,
                });
            }

            return result;
        }

        private static XDocument Sort(XDocument plyDet)
        {
            return new XDocument(
                new XElement(plyDet.Root.Name,
                from x in plyDet.Root.Elements("game")
                orderby int.Parse(x.Element("score").Value)
                select x));

            // plyDet.Root.ReplaceAll(from p in plyDet.Root.Elements("game")
            //                             orderby int.Parse(p.Element("score").Value)
            //                             select p);
        }
    }
}
