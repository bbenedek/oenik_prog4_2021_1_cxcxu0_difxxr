﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1116:Split parameters should start on line after declaration", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.Sort(System.Xml.Linq.XDocument)~System.Xml.Linq.XDocument")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:Parameter should not span multiple lines", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.Sort(System.Xml.Linq.XDocument)~System.Xml.Linq.XDocument")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.Sort(System.Xml.Linq.XDocument)~System.Xml.Linq.XDocument")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "CA1014:Mark assemblies with CLSCompliant", Justification = "Not neeeded in this project.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.LastSave~AsteroidDweller.GameModel.GameModel")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.TopList~System.Collections.Generic.List{AsteroidDweller.GameModel.Player}")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.SaveGame(AsteroidDweller.GameModel.GameModel)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.SaveTopList(AsteroidDweller.GameModel.Player)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.LastSave~AsteroidDweller.GameModel.GameModell")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:AsteroidDweller.StorageRepository.DStorageRepository.SaveGame(AsteroidDweller.GameModel.GameModell)")]
