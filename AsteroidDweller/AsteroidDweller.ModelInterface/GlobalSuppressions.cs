﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "SA0001", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "MSB3245", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "MSB3243", Justification = "<NikGitStats>", Scope = "module")]

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Not needed in this project.", Scope = "member", Target = "~P:AsteroidDweller.GameModelInterfaces.IGameModel.Asteroids")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Not needed in this project.", Scope = "member", Target = "~P:AsteroidDweller.GameModelInterfaces.IGameModel.Asteroids")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "CA1014:Mark assemblies with CLSCompliant", Justification = "Not neeeded in this project.")]