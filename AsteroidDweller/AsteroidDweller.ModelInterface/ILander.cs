﻿// <copyright file="ILander.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    /// <summary>
    /// Interface for defining the space ship object.
    /// </summary>
    public interface ILander : IGameObject
    {
        /// <summary>
        /// Gets or sets horizontal acceleration change based on user input.
        /// </summary>
        public double DX { get; set; }

        /// <summary>
        /// Gets or sets vertical acceleration change based on user input.
        /// </summary>
        public double DY { get; set; }

        /// <summary>
        /// Method for reseting the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public void ResetInstance(double newcx, double newcy, int layer)
        {
        }

        /// <summary>
        /// Method for rotating the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        public void RotateInstance(double newcx, double newcy)
        {
        }
    }
}
