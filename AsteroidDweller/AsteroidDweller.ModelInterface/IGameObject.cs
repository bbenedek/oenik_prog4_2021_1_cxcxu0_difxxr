﻿// <copyright file="IGameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    using System.Windows.Media;

    /// <summary>
    /// Interface for defining the base game object.
    /// </summary>
    public interface IGameObject
    {
        /// <summary>
        /// Gets or sets layer.
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets X coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets rotation.
        /// </summary>
        public double Rotation { get; set; }

        /// <summary>
        /// Gets shape.
        /// </summary>
        public Geometry Shape { get; }

        /// <summary>
        /// Collidation check.
        /// </summary>
        /// <param name="other">It waits an IgameObject.</param>
        /// <returns>REturns collidation check.</returns>
        public bool CollidesWith(IGameObject other);
    }
}
