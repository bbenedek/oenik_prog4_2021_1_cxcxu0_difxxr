﻿// <copyright file="IPlayer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    /// <summary>
    /// Interface for defining the Player (name and score).
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Gets or sets the score of the player.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        string Name { get; set; }
    }
}
