﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for defining the GameModel object.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the Anomaly.
        /// </summary>
        IAnomaly Anomaly { get; set; }

        /// <summary>
        /// Gets or sets the list of the Asteroids.
        /// </summary>
        List<IAsteroid> Asteroids { get; set; }

        /// <summary>
        /// Gets or sets the list of the Asteroida.
        /// </summary>
        IAsteroid Asteroid { get; set; }

        /// <summary>
        /// Gets or sets the Lander.
        /// </summary>
        ILander Lander { get; set; }

        /// <summary>
        /// Gets or sets the Platform.
        /// </summary>
        IPlatform Platform { get; set; }

        /// <summary>
        /// Gets or sets the Player.
        /// </summary>
        IPlayer Player { get; set; }

        /// <summary>
        /// Gets or sets the WindowWidth.
        /// </summary>
        double WindowWidth { get; set; }

        /// <summary>
        /// Gets or sets the WindowHeight.
        /// </summary>
        double WindowHeight { get; set; }
    }
}
