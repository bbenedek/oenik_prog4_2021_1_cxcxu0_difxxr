﻿// <copyright file="IAnomaly.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    /// <summary>
    /// Interface for defining the Anomaly object.
    /// </summary>
    public interface IAnomaly : IGameObject
    {
        /// <summary>
        /// Method for reseting the instance.
        /// </summary>
        /// <param name="newcx">new X coordinate.</param>
        /// <param name="newcy">new Y coordinate.</param>
        /// <param name="layer">Layer of the object.</param>
        public void ResetInstance(double newcx, double newcy, int layer)
        {
        }
    }
}
