﻿// <copyright file="IPlatform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.GameModelInterfaces
{
    /// <summary>
    /// Interface for defining the Platform object.
    /// </summary>
    public interface IPlatform : IGameObject
    {
    }
}
