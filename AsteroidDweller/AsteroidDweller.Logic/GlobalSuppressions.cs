﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Not imortant in this project.", Scope = "member", Target = "~M:AsteroidDweller.Logic.GameLogic.AsteroidTic(AsteroidDweller.GameModelInterfaces.IAsteroid)")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Not imortant in this project.", Scope = "member", Target = "~M:AsteroidDweller.Logic.GameLogic.NewAnomaly")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Not imortant in this project.", Scope = "member", Target = "~M:AsteroidDweller.Logic.GameLogic.NewAsteroid")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "CA1014:Mark assemblies with CLSCompliant", Justification = "Not neeeded in this project.")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Not imortant in this project.", Scope = "member", Target = "~M:AsteroidDweller.Logic.GameLogic.#ctor(AsteroidDweller.GameModel.GameModell)")]
