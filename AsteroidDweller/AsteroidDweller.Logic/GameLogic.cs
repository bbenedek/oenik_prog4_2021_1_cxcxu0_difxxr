﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using AsteroidDweller.GameLogicInterfaces;
    using AsteroidDweller.GameModel;
    using AsteroidDweller.GameModelInterfaces;
    using AsteroidDweller.StorageRepository;

    /// <summary>
    /// Class representing the GameLogic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Random number generation for setting anomaly and asteroids.
        /// </summary>
        private static Random rnd = new Random();

        /// <summary>
        /// New IGameModel instance.
        /// </summary>
        private GameModell gameModel;

        /// <summary>
        /// New IStorage instance.
        /// </summary>
        private DStorageRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">GameModel instance.</param>
        public GameLogic(GameModell gameModel)
        {
            this.gameModel = gameModel;

            this.repository = new DStorageRepository();

            this.GameIsOver = false;

            this.gameModel.Player = new Player();

            // Test end.
            this.gameModel.Asteroids = new List<IAsteroid>();

            /*Set the Anomaly's starting position and layer.*/
            this.gameModel.Anomaly = new Anomaly(this.gameModel.WindowWidth / 2, this.gameModel.WindowHeight / 2, rnd.Next(0, 2));

            /*Set the Lander's starting position and layer.*/
            this.gameModel.Lander = new Lander(15, this.gameModel.WindowHeight / 2);

            /*Set the Platform's starting position and layer.*/
            this.gameModel.Platform = new Platform(0, (this.gameModel.WindowHeight / 2) + 40);
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets ors sets the boolean gameover.
        /// </summary>
        public bool GameIsOver { get; set; }

        /// <summary>
        /// Sets the player's name and score.
        /// </summary>
        /// <param name="playerName">Name.</param>
        /// <param name="playerScore">Score.</param>
        public void SetPlayer(string playerName, int playerScore)
        {
            this.gameModel.Player.Name = playerName;

            this.gameModel.Player.Score = playerScore;
        }

        /// <summary>
        /// Sets the lander's horizontal and vertical accelaration based on user input.
        /// </summary>
        /// <param name="deltaX">Horizontal acceleration.</param>
        /// <param name="deltaY">Vertical acceleration.</param>
        public void LanderBoost(int deltaX, int deltaY)
        {
            this.gameModel.Lander.DX += deltaX;
            this.gameModel.Lander.DY += deltaY;
        }

        /// <summary>
        /// Sets the new coordinates of the Asteroid whenever a tick happens.
        /// </summary>
        /// <param name="asteroid">Asteroid to be moved.</param>
        public void AsteroidTic(IAsteroid asteroid)
        {
            if (asteroid != null)
            {
                asteroid.CX += 10;

                if (asteroid.CX > this.gameModel.WindowWidth)
                {
                    asteroid.ResetInstance(5, rnd.Next(20, (int)(this.gameModel.WindowHeight - 20)), rnd.Next(0, 2));
                }
            }
        }

        /// <summary>
        /// Rotates the lander and sets the lander's position based on the horizontal and vertical accelaration, plus gravitation.
        /// </summary>
        public void LanderTick()
        {
            this.gameModel.Lander.CX += this.gameModel.Lander.DX;
            this.gameModel.Lander.CY += this.gameModel.Lander.DY;

            /*Gravitaty.*/
            this.gameModel.Lander.DY++;

            /*Needs to be checked.*/
            this.gameModel.Lander.Rotation = Math.Atan2(this.gameModel.Lander.DY, this.gameModel.Lander.DX);
        }

        /// <summary>
        /// Method for calling a game turn, where objects move and possibly collide.
        /// </summary>
        public void DoTurn()
        {
            /*Layer check not needed.*/
            if (this.gameModel.Lander.CollidesWith(this.gameModel.Platform))
            {
                this.gameModel.Lander.CX = 15;
                this.gameModel.Lander.CY = this.gameModel.WindowHeight / 2;
                this.gameModel.Lander.DY = 0;
                this.gameModel.Lander.Rotation = 0;
            }
            else
            {
                this.LanderTick();

                if (this.gameModel.Lander.CX > this.gameModel.WindowWidth || this.gameModel.Lander.CX < 0 || this.gameModel.Lander.CY > this.gameModel.WindowHeight || this.gameModel.Lander.CY < 0)
                {
                    this.GameOver();
                }
            }

            if (this.gameModel.Lander.Layer == this.gameModel.Anomaly.Layer && this.gameModel.Lander.CollidesWith(this.gameModel.Anomaly))
            {
                this.gameModel.Player.Score++;

                this.NewAnomaly();

                this.NewAsteroid();

                this.gameModel.Platform.CX = 10000;
                this.gameModel.Platform.CY = 10000;
            }

            if (this.gameModel.Asteroids.Count != 0)
            {
                foreach (IAsteroid asteroid in this.gameModel.Asteroids)
                {
                    this.AsteroidTic(asteroid);

                    if (this.gameModel.Lander.Layer == asteroid.Layer && asteroid.CollidesWith(this.gameModel.Lander))
                    {
                        this.GameOver();
                    }
                }
            }
        }

        /// <summary>
        /// Ends the current game session, and calls the UpdateToplist(IPlayer player).
        /// </summary>
        public void GameOver()
        {
            /*TODO close dialog?*/
            this.UpdateToplist();
            this.GameIsOver = true;
        }

        /// <summary>
        /// Initializes a new instance of the GameLogic class, based on a saved game.
        /// </summary>
        public void LoadGame()
        {
            this.gameModel = this.repository.LastSave();
        }

        /// <summary>
        /// Method for setting the Anomaly's new location.
        /// </summary>
        public void NewAnomaly()
        {
            this.gameModel.Anomaly.ResetInstance(rnd.Next(50, (int)(this.gameModel.WindowWidth - 50)), rnd.Next(50, (int)(this.gameModel.WindowHeight - 50)), rnd.Next(0, 2));
        }

        /// <summary>
        /// Method for adding a new Asteroid to the asteroid list.
        /// </summary>
        public void NewAsteroid()
        {
            Asteroid asteroid = new Asteroid(5, rnd.Next(20, (int)(this.gameModel.WindowHeight - 20)), rnd.Next(0, 2));

            this.gameModel.Asteroids.Add(asteroid);
        }

        /// <summary>
        /// Saves the current GameModel instance details.
        /// </summary>
        public void SaveGame()
        {
            this.repository.SaveGame(this.gameModel);
        }

        /// <summary>
        /// Saves the player's name an score to the toplist.
        /// </summary>
        public void UpdateToplist()
        {
            this.repository.SaveTopList((Player)this.gameModel.Player);
        }

        /// <summary>
        /// Shows the players results.
        /// </summary>
        /// <returns>player name and score.</returns>
        public string Toplist()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in this.repository.TopList())
            {
                sb.AppendLine(item.Name + " \n" + item.Score);
            }

            return sb.ToString();
        }
    }
}
