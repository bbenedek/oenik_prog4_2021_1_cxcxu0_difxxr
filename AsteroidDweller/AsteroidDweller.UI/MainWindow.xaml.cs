﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AsteroidDweller.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using AsteroidDweller.DwellerControl;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// MAin window for xaml.cs.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void GameDetails_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("A játék célja, hogy minél több értékes (zöld) aszteroidát tudjunk összegyűjteni, anélkül, hogy ütköznénk a pálya szélével, vagy a veszélyes aszteroidákkal.\nAz űrhajó 1 első, 2 hátsó, 1 felső, és 5 alsó hajtóművel rendelkezik, ezekkel ellentétes irányú gyorsulás érhető el.\nAz emelkedést gravitáció nehezíti.\nA hajtóműveket a klaviatúra nyilaival lehet működtetni.");
        }

        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            GameWindow gameWindow = new GameWindow();
            AsteroidDwellerControl control = this.DataContext as AsteroidDwellerControl;

            control.PlayerName = this.PlayerName.Text;

            gameWindow.Show();
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Köszönöm, hogy kipróbálta a játékot.\nAz OK gombra kattintva biztonságosan bezárható az alkalmazás.");
            this.Close();
        }

        private void LoadGame_Click(object sender, RoutedEventArgs e)
        {
            // TODO: implent the LoadGame to the Control via Logic with events?
            AsteroidDwellerControl contorl = new AsteroidDwellerControl();
            contorl.LoadGame();
        }

        private void Toplist_Click(object sender, RoutedEventArgs e)
        {
            AsteroidDwellerControl contorl = new AsteroidDwellerControl();
            contorl.Toplist();
            MessageBox.Show($"{contorl.ToplistToShow()}\nToplist Ok!");
            this.Close();
        }

        private void SaveGame_Click(object sender, RoutedEventArgs e)
        {
            AsteroidDwellerControl contorl = new AsteroidDwellerControl();
            contorl.SaveGame();
            MessageBox.Show("Save Ok");
            this.Close();
        }
    }
}
